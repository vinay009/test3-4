//
//  GameScene.swift
//  Test34-S19
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 rabbit. All rights reserved.
//

import SpriteKit
import GameplayKit

class Level2: SKScene,SKPhysicsContactDelegate {
    
    var player:SKNode!
    var exit:SKNode!
    var Block:SKNode!
    var nextLevelButton:SKLabelNode!
    
    
    override func didMove(to view: SKView) {
        print("Loaded level 2")
        self.nextLevelButton = self.childNode(withName: "nextLevelButton") as! SKLabelNode
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.physicsWorld.contactDelegate = self
         self.player = self.childNode(withName: "player")
    }
    
    func makeBlock(xPosition:CGFloat, yPosition:CGFloat) {
        
        let Block = SKSpriteNode(imageNamed: "block")
        
        Block.physicsBody?.categoryBitMask = 4
        Block.physicsBody?.affectedByGravity = false
        Block.physicsBody?.isDynamic = true
        
        Block.position.x = xPosition
        Block.position.y = yPosition
        
       
        
        addChild(Block)
        
    }
    func spawnlemmings() {
        // 1. create a sand node
        let lemmings = SKSpriteNode(imageNamed: "player")
        lemmings.zPosition = -1
        let action = SKAction.moveTo(y: self.size.width + 100, duration: 0.1)
        
        
        // 2. set the initial position of the sand
        lemmings.position =  CGPoint(x:30, y: 1200)  //  x = w/2
        //lemmings.position.y = self.position.entrancer// y = h-100
        
        // 3. add physics body to sand
        lemmings.physicsBody = SKPhysicsBody(circleOfRadius:lemmings.size.width/2)
        lemmings.physicsBody?.categoryBitMask = 16
        lemmings.physicsBody?.contactTestBitMask = 8
        lemmings.physicsBody?.isDynamic = true
        lemmings.physicsBody?.affectedByGravity = true
      
            
        
        // By default, dynamic = true, gravity = true
        // You don't need to change the defaults because
        // we  want the sand to fall down & collide into things
        
        // 4. add the sand to the scene
        addChild(lemmings)
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        print("you win")
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        self.spawnlemmings()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // 1. detect where the person clicked
        let touch = touches.first!
        let mousePosition = touch.location(in:self)
        
        // 2. make an orange in the same position as mouse click
        self.makeBlock(xPosition: mousePosition.x, yPosition: mousePosition.y)
        
        // MARK: Switch Levels
        
        if (SKNode().name == "nextLevelButton") {
            let scene = SKScene(fileNamed:"Level3")
            if (scene == nil) {
                print("Error loading level")
                return
            }
            else {
                scene!.scaleMode = .aspectFill
                view?.presentScene(scene!)
            }
        }
        
    }
}
