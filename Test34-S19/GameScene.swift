//
//  GameScene.swift
//  Test34-S19
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 rabbit. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var nextLevelButton:SKLabelNode!
    
    
    override func didMove(to view: SKView) {
        print("This is level 1")
        self.nextLevelButton = self.childNode(withName: "nextLevelButton") as! SKLabelNode
        
    }
    var mouseStartingPosition:CGPoint = CGPoint(x:0, y:0)
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let mousePosition = touch.location(in:self)
        
        let location = touch.location(in:self)
        let node = self.atPoint(location)
        let spriteTouched = self.atPoint(mousePosition)
    
        // 2b. If person click tree, then make orange
        if (spriteTouched.name == "hair") {
            // make an orange in the same position as mouse click
            removeFromParent()
        }
    
        // MARK: Switch Levels
        if (node.name == "nextLevelButton") {
            let scene = SKScene(fileNamed:"Level2")
            if (scene == nil) {
                print("Error loading level")
                return
            }
            else {
                scene!.scaleMode = .aspectFill
                view?.presentScene(scene!)
            }
            override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
                
                // 1. detect where the person lifted finger up
                let touch = touches.first!
                let mousePosition = touch.location(in:self)
                
                print("Starting position: \(mouseStartingPosition.x), \(mouseStartingPosition.y)")
                print("Ending position: \(mousePosition.x), \(mousePosition.y)")
                
                
                
                
                
                // make an orange in the same position as mouse click
                //        self.makeOrange(
                //            xPosition:mousePosition.x,
                //            yPosition:mousePosition.y)
                //
                
            } // end touches ended
        }
        
    }
}
